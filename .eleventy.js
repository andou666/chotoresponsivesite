module.exports = (eleventyConfig) => {
    
    eleventyConfig.addPassthroughCopy('src')
    eleventyConfig.setUseGitIgnore(false)
  
    return {
      htmlTemplateEngine: "liquid",
      dir: {
        input: 'src',
        output: 'public',
      },
    }
  }